![plot](./pics/igcd-logo.png)

# IGCD

## Projet Description
**Igcd** , the **I**gc **D**ownloader **D**aemon, allows you to download and store locally IGC files from [https://flightbook.glidernet.org](https://flightbook.glidernet.org) or from others flightbook instances based on [flightbook source code](https://gitlab.com/lemoidului/ogn-flightbook).  
Igcd also makes it possible to relay flightbook events thanks to the implementation of webHooks.  
**Igcd** is developed with Golang and published under GNU GPL license. A [copy of License](./LICENSE) is joined to the repo.

## Installation
**Igcd** is designed for working as a service with a very small CPU and Memory footprint. Thus, you can use **Igcd** on small hardware as a RaspberryPi or other low cost plateforms. Only the storage capacity of your equipment should be taken into account depending on the retention time you want to use.  
The easiest way to install **Igcd** is to download latest [Release version](https://gitlab.com/lemoidului/igcd/-/releases) which contains Binaries for: 
 - Linux_amd64
 - Linux_arm
 - Linux_arm64
 - Windows_amd64

**Once downloaded, unzip or untar archive. The same configuration file (conf.yml) is used for all plateforms binaries.**
```bash
unzip igcd-v*.zip
# or
tar -xzf igcd-v*.tar.gz
```
You can also build **Igcd** from sources (golang version 1.17) :  
```bash
git clone https://gitlab.com/lemoidului/igcd.git
cd igcd
go build -o igcd main.go
# or if GNU Make is installed:
make
# To build binaries dist packages (all plateforms):
make dist
```
## Usage
Edit, or copy and edit **conf.yml**.  
**Respect space indentations in configuration file as it's a YAML format**.  
In configuration file, igcpath can be absolute or relative. If relative, igcpath will be evaluated from binary path level. If igcpath does not exist, directory will be create.  
ids list corresponds to device beacons ids you want to record. You can obtain devices ids @ [https://flightbook.glidernet.org](https://flightbook.glidernet.org).  
Once conf.yml edited, launch Igcd:
```bash
# show cli options
$ ./igcd --help
__________  ______   ____   ____ 
 ____  __/ / ____/ / ___/  / __ \
  __  /   / / __  / /     / / / /
  _/ /_  / /_/ / / /___  / /_/ / 
 /____/  \____/  \____/ /_____/  

https://gitlab.com/lemoidului/igcd
Version     : v0.1.3
Build       : 2022-01-09T11:48:58

Usage of ./igcd:
  -c string
        config file path (default "./igcd/conf.yml")
  -debug
        log debug mode
  -v    display version

# launch igcd
$ ./igcd
INFO[2022-01-10T22:52:06+01:00] Reading configuration from:  ./igcd/conf.yml 
INFO[2022-01-10T22:52:06+01:00] configuration ok, 3 Ids provided             
INFO[2022-01-10T22:52:06+01:00] Igc will be store in:  ./igcd/data 
INFO[2022-01-10T22:52:06+01:00] try connecting websocket: wss://flightbook.glidernet.org/ws 
INFO[2022-01-10T22:52:06+01:00] websocket connected, awaiting events for provided Ids 

# use [CTRL] + C for stopping igcd
^C
WARN[2022-01-10T22:52:08+01:00] Interrupt, igcd will stop

```
## WebHook
dedicated documentation concerning webhook is available [here](./Webhook.md).

## Execute in Background:
**_Igcd is designed to work as a service (ie in background). For Linux Users a quick & dirty possibility is to use tmux or screen_**:
```bash
# ensure to have tmux installed on your system, apt-get install tmux or yum install tmux ...
$ tmux
./igcd
# quit tmux session with [CTRL] + B then D
# re-attach tmux session with
$ tmux a 
```
however a slightly cleaner way is to use a systemd unit
#### systemd Unit:
You can edit a systemd Unit for automatically launch igcd at startup and retrieve your logs in journalctl (Linux users only)  
Sytemd Unit template should be :
```bash
# create and edit file: /etc/systemd/system/igcd.service
[Unit]
Description=igcd daemon
After=network.target

[Service]
User=<your_user>
Group=<your_group>
WorkingDirectory=<igcd directory>
ExecStart=<igcd directory>/igcd -c <yml configuration file>
ExecStop=/bin/kill -s SIGINT $MAINPID

[Install]
WantedBy=multi-user.target

```
Once created,
```bash
# Reload systemd service manager to include the new service.
sudo systemctl daemon-reload
# Start your service
sudo systemctl start igcd.service
# To check the status of your service
sudo systemctl status igcd.service
# To enable your service on every reboot
sudo systemctl enable igcd.service
```
## Privacy
In accordance with OGN policy, flightbook does not keep any flight path nor igc for a period of more than 24 hours.  
You can however use igcd for your personal or gliding club usage with condition of **not publicly distributing files outside your private sphere**.  
Flightbook maintener could restrict network access for users who do not fully comply with this rule.  
The use of Ids which are not declared in [OGN database](ddb.glidernet.org ) (ids marked as '*unregistred*' in flightbook and starting with a *#*) is not compatible with igcd because these Ids are ephemerals.  
**Concerning ephemerals IDs, author will not modify Igcd in any case whatever the motivation of the request**.

## Develop / work in progress
 - Monitore & catch igc for aircarfts not detected as landed 
 - Add more events/webhooks
 - Amazon S3 Bucket storage option
 - and more ...  
 Do not hesitate Gophers, looking forward to sharing your skills
   
