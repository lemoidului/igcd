module igcd

go 1.17

require (
	github.com/gorilla/websocket v1.4.2
	github.com/mattn/go-colorable v0.1.12
	github.com/sirupsen/logrus v1.8.1
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/sys v0.0.0-20210927094055-39ccf1dd6fa6 // indirect
)
