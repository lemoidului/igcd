BIN = $(CURDIR)/bin
DIST = $(CURDIR)/dist
CONF = $(CURDIR)/conf.yml

$(shell mkdir -p $(BIN))
$(shell mkdir -p $(DIST))
M = $(shell printf "\033[34;1m▶\033[0m")
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null)
DATE    ?= $(shell date +%FT%T)

LDFLAGS = -w -s
LDFLAGS:=$(LDFLAGS) -X igcd/utils.Version=$(VERSION) -X igcd/utils.Build=$(DATE)
SOURCES := $(shell find $(CURDIR) -name "*.go")

$(CURDIR)/igcd: $(SOURCES)
	go build -ldflags '$(LDFLAGS)' -o $@ main.go
$(BIN)/igcd-linux-arm: $(SOURCES)
	GOOS=linux GOARCH=arm go build -ldflags '$(LDFLAGS)' -o $@ main.go
$(BIN)/igcd-linux-arm64: $(SOURCES)
	GOOS=linux GOARCH=arm64 go build -ldflags '$(LDFLAGS)' -o $@ main.go
$(BIN)/igcd-linux-amd64: $(SOURCES)
	GOOS=linux GOARCH=amd64 go build -ldflags '$(LDFLAGS)' -o $@ main.go
$(BIN)/igcd-win-amd64.exe: $(SOURCES)
	GOOS=windows GOARCH=amd64 go build -ldflags '$(LDFLAGS)' -o $@ main.go

linux-arm: $(BIN)/igcd-linux-arm
linux-arm64: $(BIN)/igcd-linux-arm64
linux-amd64: $(BIN)/igcd-linux-amd64
win: $(BIN)/igcd-win-amd64.exe

OBJS=$(BIN)/igcd-linux-arm $(BIN)/igcd-linux-arm64 $(BIN)/igcd-linux-amd64 $(BIN)/igcd-win-amd64.exe $(CONF)

$(DIST)/igcd-$(VERSION).zip: $(OBJS)
	$(eval ZIP=$(notdir $@))
	@mkdir -p $(DIST)/$(VERSION)
	@cp -r $(BIN)/* $(DIST)/$(VERSION)
	@cp $(CONF) $(DIST)/$(VERSION)
	(cd $(DIST) ; zip -r $(ZIP) $(VERSION) -x './data' ; chmod 774 $(ZIP))
	

$(DIST)/igcd-$(VERSION).tar.gz: $(DIST)/igcd-$(VERSION).zip
	$(eval TAR=$(notdir $@))
	(cd $(DIST) ; tar --exclude='./data' -czf $(TAR) $(VERSION) ; chmod 774 $(TAR))
	

build: $(CURDIR)/igcd

compile: linux-arm linux-arm64 linux-amd64 win

dist: $(DIST)/igcd-$(VERSION).zip $(DIST)/igcd-$(VERSION).tar.gz


clean: ; $(info $(M) cleaning …)
	@rm -rf $(BIN)
	@rm -rf $(DIST)
	-@rm $(CURDIR)/igcd

version:
	@echo $(VERSION)

all: build dist

.PHONY: build all dist compile version clean
