# Webhooks
**_A webhook in web development is a method of augmenting or altering the behavior of a web page or web application with custom callbacks. These callbacks may be maintained, modified, and managed by third-party users and developers who may not necessarily be affiliated with the originating website or application. The term "webhook" was coined by Jeff Lindsay in 2007 from the computer programming term hook.  
<cite>[wikipedia]</cite>_**

## In which case Igcd webhook could help you ?
Imagine that as the maintainer of your Gliding Club website, you want to act in real time and triggers actions based on flightbook.glidernet.org events (actualize some specific display, billing system, alerting, etc ...).    
The first option is to do continuous polling on the flightbook API endpoint. It is neither efficient nor clean. Moreover, there is a higher chance that you will be blocked because you are unnecessarily soliciting ressources.  
As we are no longer in 1990 and the good old crontab or equivalent methods should be banned as they are so ugly, you can (and you should) use webhooks.  
Igcd webhooks will relay events in real time on a dedicated url endpoint on your own Website / Api.  

## Webhook HTTP messages
A webhook message is tipically an **HTTP POST** request with a json payload sent in the **BODY** request.  
Content-type Header is set to **"application/json"**.  
Json payload depends of events.

## Webhook events
  
- ## testing webhook
    Sent each time igcd is launched to check on your Website / Api side.  
    Message contains your webhooks configuration.  
    ```json
    {
    "type": "testhook",
    "origin": "igcd v0.2.2",
    "config": {
        "active": true,
        "url": "http://127.0.0.1:8080",
        "headers": {
            "Whatever": "",
            "X-Api-Key": "secret"
        },
        "airfields": ["LFNL"]
        "triggers": {
            "landing": true,
            "takeoff": true,
            "udlanding": true,
            "udtakeoff": true
        }
    }
    ``` 
- ## landing webhook
    ```json
    {"type":"landing","origin":"igcd v0.2.2","airfield":"LFNL","id":"DDB2AB"}
    ```
- ## takeoff webhook
    ```json
    {"type":"takeoff","origin":"igcd v0.2.2","airfield":"LFNL","id":"DDB2AB"}   
    ```
- ## undeclared landing webhook
    Sent for an aircraft not defined in Ids list when landing occurs in airfields list 
    ```json
    {"type":"udlanding","origin":"igcd v0.2.2","airfield":"LFNL","id":"XXXXXX"}
    ```
- ## undeclared takeoff webhook
    Sent for an aircraft not defined in ids list when takeoff occurs in airfields list 
    ```json
    {"type":"udtakeoff","origin":"igcd v0.2.2","airfield":"LFNL","id":"XXXXXX"}
    ```

## WebHook configuration
```yml
webhook:
    active: true
    url: "http://127.0.0.1:8080"
    delay: 0
    headers:
        "X-Api-Key": "secret"
        "Whatever": "something"
    airfields:
        - LFNL
    triggers:
        takeoff: true
        landing: true
        udtakeoff: true
        udlanding: true
```
- You can add has many headers fields as you want in your configuration.  
- The delay value represents the duration time in seconds between event and webhook notification.

