package ws

import (
	"context"
	"encoding/json"
	"errors"
	"time"

	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
)

// server send ping control message each 27 seconds
// if nothing is received during 30 seconds will try to disconnect / reconnect
const pingProbe = 30 * time.Second

type client struct {
	url       string
	conn      *websocket.Conn
	Ctx       context.Context
	ctxCancel context.CancelFunc
}

type msg struct {
	Type int         `json: "type"`
	Data interface{} `json: "data"`
}

type Evt struct {
	Code string `json: "code"`
	Evt  string `json: "evt"`
	Addr string `json: "addr"`
}

func NewClient(url string) (c client) {
	c.url = url
	c.Ctx, c.ctxCancel = context.WithCancel(context.Background())
	return c
}

func (c *client) connect() error {
	if c.conn != nil {
		return nil
	}
	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()
	var err error
	for ; ; <-ticker.C {
		select {
		case <-c.Ctx.Done():
			return errors.New("Context done")
		default:
			log.Infoln("try connecting websocket:", c.url)
			c.conn, _, err = websocket.DefaultDialer.Dial(c.url, nil)
			if err != nil {
				log.Errorln("websocket connection error:", err)
				continue
			}
			log.Infoln("websocket connected, awaiting events for provided Ids")
			// detect server disconnect by setting connection ReadDeadLine
			// ReadDeadline will be intialize each time a Ping is received
			c.conn.SetReadDeadline(time.Now().Add(pingProbe))
			c.conn.SetPingHandler(func(string) error {
				log.Debugln("Ping received")
				c.conn.SetReadDeadline(time.Now().Add(pingProbe))
				return nil
			})
			return nil
		}
	}
}

func (c *client) GetEvent() (*Evt, error) {
	var event *Evt
	for {
		err := c.connect()
		if err != nil {
			return nil, err
		}
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			select {
			case <-c.Ctx.Done():
				return nil, err
			default:
				log.Errorln("read socket error, client will be close")
				c.close()
				return nil, err
			}
		}
		log.Debugf("msg received: %s", message)
		var msg msg
		err = json.Unmarshal(message, &msg)
		if err != nil {
			log.Warn("read message error")
			continue
		}
		if msg.Type == 1 {
			var jdata []byte
			jdata, err = json.Marshal(msg.Data)
			if err != nil {
				log.Warn("read Event error")
				continue
			}
			err = json.Unmarshal(jdata, &event)
			if err != nil {
				log.Warnln("read Event error")
				continue
			}
			return event, nil
		}
	}
}

func (c *client) close() {
	if c.conn != nil {
		log.Debugln("Sending close message to server")
		c.conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
		log.Debugln("Closing connection")
		c.conn.Close()
		c.conn = nil
	}
}

func (c *client) Stop() {
	c.ctxCancel()
	c.close()
}
