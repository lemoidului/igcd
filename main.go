package main

import (
	"flag"
	"fmt"
	"igcd/utils"
	"igcd/ws"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"time"

	"github.com/mattn/go-colorable"
	log "github.com/sirupsen/logrus"
)

func main() {
	// first create default configuration file (case -c not provided in cmd)
	// in this case, "conf.yml" will be taken @ binary path level
	ex, err := os.Executable()
	if err != nil {
		log.Fatal(err)
	}
	exPath := filepath.Dir(ex)
	def_cfgfile := filepath.Join(exPath, "conf.yml")

	cfgfile := flag.String("c", def_cfgfile, "config file path")
	debug := flag.Bool("debug", false, "log debug mode")
	version := flag.Bool("v", false, "display version")

	fmt.Print(utils.Banner)
	fmt.Printf("%s\n", utils.Repo)
	fmt.Printf("Version     : %s\n", utils.Version)
	fmt.Printf("Build       : %s\n\n", utils.Build)

	flag.Parse()
	if *version {
		return
	}
	goos := runtime.GOOS
	switch goos {
	case "windows":
		log.SetFormatter(&log.TextFormatter{ForceColors: true, FullTimestamp: true})
		log.SetOutput(colorable.NewColorableStdout())
	default:
		log.SetFormatter(&log.TextFormatter{FullTimestamp: true})
	}
	if *debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}

	var cfg utils.Config
	log.Infoln("Reading configuration from: ", *cfgfile)
	uniqIds := utils.Readcfg(&cfg, *cfgfile)

	ws_url, api_url := cfg.GetUrl()
	log.Infof("configuration read, %d Ids provided", len(uniqIds))

	// if igc data directory specified in config file is relative
	// data directory path will be evaluated from binary path level
	if !filepath.IsAbs(cfg.Igcpath) {
		cfg.Igcpath = filepath.Join(exPath, cfg.Igcpath)
	}
	log.Infoln("Igc will be store in: ", cfg.Igcpath)

	// if WebHook active, send a WebHook test
	if cfg.WebHook.Isactive() {
		log.Infoln("Testing Hook configuration")
		hook := utils.HookTest(cfg.WebHook)
		hook.Send()
	}

	client := ws.NewClient(ws_url)
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)
	go func() {
		select {
		case <-interrupt:
			log.Warnln("Interrupt, igcd will stop")
			client.Stop()
		}
	}()

	var udId bool // undeclared Id => Id not in conf Ids list but triggered by airfields hook
	for {
		select {
		case <-client.Ctx.Done():
			time.Sleep(time.Second)
			return
		default:
			udId = false
			event, err := client.GetEvent()
			if err != nil {
				continue
			}

			log.Debugln("event", *event)
			// check Id && Airfields Hooks
			if _, ok := uniqIds[event.Addr]; !ok {
				if !cfg.WebHook.Isactive() {
					continue
				}
				if cfg.WebHook.AirfieldList(event.Code) {
					udId = true
				} else {
					continue
				}
			}

			switch event.Evt {
			// takeoff case
			case "1":
				if cfg.WebHook.Isactive() {
					if udId {
						if hook := utils.HookLdgTof("udtakeoff", event.Addr, event.Code, cfg.WebHook); hook != nil {
							go hook.Send()
						}
					} else {
						if hook := utils.HookLdgTof("takeoff", event.Addr, event.Code, cfg.WebHook); hook != nil {
							go hook.Send()
						}
					}
				}
			// landing case
			case "0":
				if cfg.WebHook.Isactive() {
					if udId {
						if hook := utils.HookLdgTof("udlanding", event.Addr, event.Code, cfg.WebHook); hook != nil {
							go hook.Send()
						}
					} else {
						if hook := utils.HookLdgTof("landing", event.Addr, event.Code, cfg.WebHook); hook != nil {
							go hook.Send()
						}
					}
				}
				// download igc only for declared id
				if !udId {
					log.Infof("%s @ %s will be download in 15 seconds\n", event.Addr, event.Code)
					url := fmt.Sprintf("%s/%s/0", api_url, event.Addr)
					go utils.DownloadIgc(client.Ctx, url, cfg.Igcpath, event.Code)
				}
			}
		}
	}
}
