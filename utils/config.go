package utils

import (
	"net/url"
	"os"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

// Version & Build date dynamically overwriten during build with -ldflags
var Repo = "https://gitlab.com/lemoidului/igcd"
var Version = "dev"
var Build = "2022-01-03"

type Config struct {
	Instance string      `yaml:"instance"`
	Igcpath  string      `yaml:"igcpath"`
	WebHook  *webHookCfg `yaml:"webhook,omitempty"`
	Ids      []string    `yaml:"ids"`
}

func Readcfg(cfg *Config, path string) map[string]bool {
	f, err := os.Open(path)
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Fatal("Error reading config file")
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(cfg)
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Fatal("Error config file format")
	}

	if cfg.WebHook == nil {
		log.Warn("config file do not contains WebHook conf. WebHook deactivate")
	} else if !cfg.WebHook.Active {
		log.Info("Webhook configuration not active")
	}

	uniqIds := make(map[string]bool)
	for _, id := range cfg.Ids {
		if _, ok := uniqIds[id]; ok {
			log.Warnf("id %s is duplicated, check configuration file", id)
		}
		uniqIds[id] = true
	}
	return uniqIds
}

func (cfg *Config) GetUrl() (string, string) {
	uinst, err := url.Parse(cfg.Instance)
	if err != nil {
		log.Fatalln("bad instance url format:", cfg.Instance)
	}
	var ws_scheme string
	if uinst.Scheme == "http" {
		ws_scheme = "ws"
	} else if uinst.Scheme == "https" {
		ws_scheme = "wss"
	} else {
		log.Fatalln("bad instance url format:", cfg.Instance)
	}

	if cfg.WebHook != nil && cfg.WebHook.Active {
		if cfg.WebHook.Url == "" {
			log.Warnln("WebHook active with empty url, deactivate")
			cfg.WebHook.Active = false
		} else {
			uwhook, err := url.Parse(cfg.WebHook.Url)
			if err != nil {
				log.Fatalln("bad instance webHook url format:", cfg.WebHook.Url)
			}
			if (uwhook.Scheme != "http") && (uwhook.Scheme != "https") || (uwhook.Host == "") {
				log.Fatalln("bad instance webHook url format:", cfg.WebHook.Url)
			}
		}
	}
	wsUrl := url.URL{Scheme: ws_scheme, Host: uinst.Host, Path: "/ws"}
	apiUrl := url.URL{Scheme: uinst.Scheme, Host: uinst.Host, Path: "/api/live/igc"}
	return wsUrl.String(), apiUrl.String()
}
