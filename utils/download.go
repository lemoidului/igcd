package utils

import (
	"context"
	"errors"
	"fmt"
	"io"
	"mime"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

func DownloadIgc(ctx context.Context, url string, store string, airfield string) {
	select {
	case <-ctx.Done():
		log.Warnln("Download cancel")
		return
	case <-time.After(15 * time.Second):
		// wait 15 seconds before downloading to obtain "full" igc file
		log.Infoln("Download start")
	}
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Errorln(err)
		return
	}
	req.Header.Set("User-Agent", fmt.Sprintf("OGN igcd %s", Version))
	resp, err := client.Do(req)
	if err != nil {
		log.Errorln(err)
		return
	}
	defer resp.Body.Close()
	log.Infoln("HTTP Response Status:", resp.StatusCode, http.StatusText(resp.StatusCode))
	if resp.StatusCode == 200 {
		// Create the file
		cd := resp.Header.Get("Content-Disposition")
		if cd == "" {
			err := errors.New("empty content disposition")
			log.Errorln(err)
			return
		}
		_, params, err := mime.ParseMediaType(cd)
		if err != nil {
			log.Errorln(err)
			return
		}
		filename, ok := params["filename"]
		if !ok {
			err = errors.New("empty filename")
			log.Errorln(err)
			return
		}
		dpath, err := createdir(store, airfield, filename)
		if err != nil {
			log.Fatal(err)
		}
		fpath := fmt.Sprintf("%s/%s", dpath, filename)
		fpath = rename(fpath)
		out, err := os.Create(filepath.FromSlash(fpath))
		if err != nil {
			log.Fatal(err)
			return
		}
		defer out.Close()
		// Write the body to file
		_, err = io.Copy(out, resp.Body)
	} else if resp.StatusCode == 204 {
		log.Info("Empty response")
	} else {
		log.Infoln("Response status:", resp.Status)
	}
}

func createdir(store string, airfield string, filename string) (dpath string, err error) {
	// create directory based on date in filename
	// 2021-12-21_[....].igc -> store/2021/12/21/airfield
	var year, month, day string
	re := regexp.MustCompile(`^(\d{4})-(\d{2})-(\d{2})`)
	submatch := re.FindStringSubmatch(filename)
	if len(submatch) == 4 {
		year, month, day = submatch[1], submatch[2], submatch[3]
	} else {
		err = errors.New("filename bad date format")
		return
	}
	dpath = fmt.Sprintf("%s/%s/%s/%s/%s", store, year, month, day, airfield)
	err = os.MkdirAll(filepath.FromSlash(dpath), 0755)
	return
}

func rename(fpath string) string {
	// rename func calculate index suffix insert in file path
	// _1.igc, _2.igc , ...
	dir, filename := filepath.Split(fpath)
	without_ext := strings.TrimSuffix(filename, ".igc")
	f, err := os.Open(filepath.FromSlash(dir))
	if err != nil {
		return fpath
	}
	files, err := f.Readdir(0)
	if err != nil {
		log.Errorln("error", err)
	}
	i, j := 0, 0
	for _, f := range files {
		re := regexp.MustCompile(fmt.Sprintf("%s_(\\d+)\\.igc$", without_ext))
		submatch := re.FindStringSubmatch(f.Name())
		if len(submatch) == 2 {
			if j, _ = strconv.Atoi(submatch[1]); j > i {
				i = j
			}
		}
	}
	return fmt.Sprintf("%s%s_%02d.igc", dir, without_ext, i+1)
}
