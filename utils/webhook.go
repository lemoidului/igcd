package utils

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
)

type webHookCfg struct {
	Active    bool              `yaml:"active" json:"active"`
	Url       string            `yaml:"url" json:"url"`
	Headers   map[string]string `yaml:"headers,omitempty" json:"headers,omitempty"`
	Airfields []string          `yaml:"airfields,omitempty" json:"airfields,omitemtpy"`
	Triggers  map[string]bool   `yaml:"triggers,omitempty" json:"triggers,omitempty"`
	Delay     int               `yaml:"delay,omitempty" json:"delay,omitempty"`
}

// generic hook interface
type hook interface {
	Send()
}

// helpers function
func (c *webHookCfg) Isactive() bool {
	if c == nil {
		return false
	}
	return c.Active
}

func (c *webHookCfg) AirfieldList(airfield string) bool {
	if c == nil {
		return false
	}
	for _, v := range c.Airfields {
		if v == airfield {
			return true
		}
	}
	return false
}

// Hook Test
type hookTest struct {
	Type string     `json:"type"`
	Org  string     `json:"origin"`
	Conf webHookCfg `json:"config"`
	cfg  *webHookCfg
}

func HookTest(c *webHookCfg) *hookTest {
	// Initialize and return a new *hookTest type or nil
	if c == nil {
		return nil
	}
	hook := hookTest{"testhook", fmt.Sprintf("igcd %s", Version), *c, c}
	return &hook
}

func (h *hookTest) Send() {
	sendHook(h, h.cfg, true)
}

// Landing or TakeOff Hook
type hookLdgTof struct {
	Type     string `json:"type"`
	Org      string `json:"origin"`
	Airfield string `json:"airfield"`
	Id       string `json:"id"`
	cfg      *webHookCfg
}

func HookLdgTof(evt string, addr string, code string, c *webHookCfg) *hookLdgTof {
	// Initialize and return a new *hookLdgTof type or nil
	if c == nil {
		return nil
	}
	if !c.Triggers[evt] {
		return nil
	}
	hook := hookLdgTof{evt, fmt.Sprintf("igcd %s", Version), code, addr, c}
	return &hook
}

func (h *hookLdgTof) Send() {
	sendHook(h, h.cfg, false)
}

// generic sendHook() function
func sendHook(h hook, cfg *webHookCfg, test bool) {
	if test {
		log.Infoln("Sending Webhook event. POST request to:", cfg.Url)
	} else {
		time.Sleep(time.Duration(cfg.Delay) * (time.Second))
		log.Info("Sending Webhook event")
	}
	client := &http.Client{}
	jdata, _ := json.Marshal(h)
	req, err := http.NewRequest("POST", cfg.Url, bytes.NewBuffer(jdata))
	if err != nil {
		log.Errorln(err)
		return
	}
	req.Header.Set("User-Agent", fmt.Sprintf("OGN igcd %s", Version))
	req.Header.Set("Content-Type", "application/json")
	// add user provided headers
	for k, v := range cfg.Headers {
		if v != "" {
			req.Header.Set(k, v)
		}
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Errorln(err)
		return
	}
	defer resp.Body.Close()
	if test {
		log.Infoln("Webhook HTTP Response:", resp.StatusCode, http.StatusText(resp.StatusCode))
	} else {
		log.Debugln("Webhook HTTP Response:", resp.StatusCode, http.StatusText(resp.StatusCode))
	}
}
